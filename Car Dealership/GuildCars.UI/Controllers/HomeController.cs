﻿using GuildCars.Data.Factories;
using GuildCars.Models.Tables;
using GuildCars.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GuildCars.UI.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {

            var model = new IndexPageViewModel();

            var specialsModel = SpecialsRepositoryFactory.GetRepository().GetAll();
            var vehiclesModel = VehiclesRepositoryFactory.GetRepository().GetFeatured();

            model.SpecialItem = specialsModel;
            model.featuredVehicleItem = vehiclesModel;

            return View(model);
        }

        public ActionResult Contact(string vinNumber)
        {
            var contactModel = new ContactValidation();

            if (vinNumber != null && vinNumber != "")
            {
                contactModel.ContactMessage = "Please contact me with more information about VIN# " + vinNumber;

                return View(contactModel);
            }

            ViewBag.Message = "Contact Us";

            return View();
        }

        [HttpPost]
        public ActionResult Contact(ContactValidation contact)
        {
            if (ModelState.IsValid)
            {
                var contactRepo = ContactsRepositoryFactory.GetRepository();

                var newContact = new Contact();

                newContact.ContactEmail = contact.ContactEmail;
                newContact.ContactId = contact.ContactId;
                newContact.ContactMessage = contact.ContactMessage;
                newContact.ContactName = contact.ContactName;
                newContact.ContactPhone = contact.ContactPhone;

                try
                {
                    contactRepo.AddContact(newContact);
                }
                catch (Exception ex)
                {

                    throw ex;
                }

                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
            
        }

        public ActionResult Special()
        {
            var specialsModel = SpecialsRepositoryFactory.GetRepository().GetAll();

            return View(specialsModel);
        }
    }
}