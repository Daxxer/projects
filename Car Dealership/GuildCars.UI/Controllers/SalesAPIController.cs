﻿using GuildCars.Data.Factories;
using GuildCars.Models.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GuildCars.UI.Controllers
{
    public class SalesAPIController : ApiController
    {
        [Authorize(Roles = "admin,sales")]
        [Route("sales/vehicle/search")]
        [AcceptVerbs("GET")]
        public IHttpActionResult SearchNew(string quickSearch, decimal? minYear, decimal? maxYear, decimal? minPrice, decimal? maxPrice)
        {
            var repo = VehiclesRepositoryFactory.GetRepository();

            try
            {
                var parameters = new VehicleSearchParameters()
                {
                    QuickSearch = quickSearch,
                    MinYear = minYear,
                    MaxYear = maxYear,
                    MinPrice = minPrice,
                    MaxPrice = maxPrice
                };

                var result = repo.VehicleSearchAll(parameters);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
