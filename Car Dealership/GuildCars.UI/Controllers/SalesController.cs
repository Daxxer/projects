﻿using GuildCars.Data.Factories;
using GuildCars.Models.Tables;
using GuildCars.UI.Models;
using GuildCars.UI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GuildCars.UI.Controllers
{
    public class SalesController : Controller
    {
        [Authorize(Roles = "admin,sales")]
        public ActionResult Index()
        {
            var model = Utilities.InventorySearchUtilities.searchModelValues();

            return View(model);
        }

        [Authorize(Roles = "admin,sales")]
        public ActionResult Purchase(string vinNumber)
        {
            var vehicleRepo = VehiclesRepositoryFactory.GetRepository();
            var stateRepo = StatesRepositoryFactory.GetRepository();
            var purchaseTypeRepo = PurchaseTypesRepositoryFactory.GetRepository();

            var model = new PurchaseViewModel();

            model.Vehicle = vehicleRepo.GetDetails(vinNumber);
            model.States = stateRepo.GetAll();
            model.PurchaseType = purchaseTypeRepo.GetAll();

            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "admin,sales")]
        public ActionResult Purchase(PurchaseViewModel model)
        {
            if (ModelState.IsValid)
            {
                var repo = PurchasesRepositoryFactory.GetRepository();

                model.Purchase.VehicleId = model.Vehicle.VehicleId;

                model.Purchase.UserId = AuthorizeUtilities.GetUserId(this);

                model.Purchase.PurchaseDate = DateTime.UtcNow;

                repo.AddPurchase(model.Purchase);

                return RedirectToAction("Index", "sales");
            }
            else
            {
                var vehicleRepo = VehiclesRepositoryFactory.GetRepository();
                var stateRepo = StatesRepositoryFactory.GetRepository();
                var purchaseTypeRepo = PurchaseTypesRepositoryFactory.GetRepository();

                var purchaseModel = new PurchaseViewModel();

                purchaseModel.Vehicle = vehicleRepo.GetDetails(model.Vehicle.VinNumber);
                purchaseModel.States = stateRepo.GetAll();
                purchaseModel.PurchaseType = purchaseTypeRepo.GetAll();

                return View(purchaseModel);
            }
        }
    }
}