﻿using GuildCars.Models.Queries;
using GuildCars.Models.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GuildCars.UI.Models
{
    public class MakeReportViewModel
    {
        public string MakeName { get; set; }
        public IEnumerable<MakeReportItem> MakeList { get; set; }
    }
}