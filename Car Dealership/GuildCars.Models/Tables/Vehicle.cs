﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuildCars.Models.Tables
{
    public class Vehicle
    {
        public int VehicleId { get; set; }
        public int MakeId { get; set; }
        public int ModelId { get; set; }
        public int BodyStyleId { get; set; }
        public string TransmissionId { get; set; }
        public int ExteriorColorId { get; set; }
        public int InteriorId { get; set; }
        public string VehicleTypeId { get; set; }
        public decimal VehicleYear { get; set; }
        public decimal Milage { get; set; }
        public string VinNumber { get; set; }
        public decimal Price { get; set; }
        public decimal MSRP { get; set; }
        public DateTime CreateDate { get; set; }
        public string ListingDescription { get; set; }
        public bool FeatureVehicle { get; set; }
    }
}
