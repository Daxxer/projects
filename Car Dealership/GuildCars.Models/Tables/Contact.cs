﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GuildCars.Models.Tables
{
    public class Contact
    {
        [Required]
        public int ContactId { get; set; }

        public string ContactName { get; set; }

        [DataType(DataType.EmailAddress)]
        public string ContactEmail { get; set; }

        [DataType(DataType.PhoneNumber)]
        public string ContactPhone { get; set; }

        [Required]
        public string ContactMessage { get; set; }        
    }
}
