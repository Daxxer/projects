﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuildCars.Models.Queries
{
    public class MakeReportItem
    {
        public int MakeId { get; set; }
        public string MakeName { get; set; }
        public DateTime CreatedDate { get; set; }
        public string UserEmail { get; set; }
    }
}
