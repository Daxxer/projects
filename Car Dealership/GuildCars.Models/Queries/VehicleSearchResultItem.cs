﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuildCars.Models.Queries
{
    public class VehicleSearchResultItem
    {
        public int VehicleId { get; set; }
        public string MakeName { get; set; }
        public string ModelName { get; set; }
        public string BodyStyleName { get; set; }
        public string TransmissionName { get; set; }
        public string ExteriorColorName { get; set; }
        public string InteriorName { get; set; }
        public decimal VehicleYear { get; set; }
        public decimal Milage { get; set; }
        public string VehicleTypeName { get; set; }
        public string VinNumber { get; set; }
        public decimal Price { get; set; }
        public decimal MSRP { get; set; }
        public string ImageFileName { get; set; }
    }
}
