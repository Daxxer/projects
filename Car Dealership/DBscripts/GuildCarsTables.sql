Use GuildCars
GO


IF EXISTS (SELECT * FROM sys.tables WHERE name = 'Specials')
	DROP TABLE Specials
GO

IF EXISTS (SELECT * FROM sys.tables WHERE name = 'Contacts')
	DROP TABLE Contacts
GO

IF EXISTS (SELECT * FROM sys.tables WHERE name = 'Purchases')
	DROP TABLE Purchases
GO

IF EXISTS (SELECT * FROM sys.tables WHERE name = 'VehicleImages')
	DROP TABLE VehicleImages
GO

IF EXISTS (SELECT * FROM sys.tables WHERE name = 'Vehicles')
	DROP TABLE Vehicles
GO

IF EXISTS (SELECT * FROM sys.tables WHERE name = 'VehicleTypes')
	DROP TABLE VehicleTypes
GO

IF EXISTS (SELECT * FROM sys.tables WHERE name = 'PurchaseTypes')
	DROP TABLE PurchaseTypes
GO

IF EXISTS (SELECT * FROM sys.tables WHERE name = 'States')
	DROP TABLE States
GO

IF EXISTS (SELECT * FROM sys.tables WHERE name = 'Models')
	DROP TABLE Models
GO

IF EXISTS (SELECT * FROM sys.tables WHERE name = 'Makes')
	DROP TABLE Makes
GO

IF EXISTS (SELECT * FROM sys.tables WHERE name = 'BodyStyles')
	DROP TABLE BodyStyles
GO

IF EXISTS (SELECT * FROM sys.tables WHERE name = 'Transmissions')
	DROP TABLE Transmissions
GO

IF EXISTS (SELECT * FROM sys.tables WHERE name = 'ExteriorColors')
	DROP TABLE ExteriorColors
GO

IF EXISTS (SELECT * FROM sys.tables WHERE name = 'Interiors')
	DROP TABLE Interiors
GO

CREATE TABLE Makes (
	MakeId int identity(1,1) not null primary key,
	MakeName varchar(25) not null,
	CreatedDate datetime2 not null,
	UserId nvarchar(128) not null foreign key references AspNetUsers(Id)
)

CREATE TABLE Models (
	ModelId int identity(1,1) not null primary key,
	MakeId int not null foreign key references Makes(MakeId),
	ModelName varchar(25) not null,
	CreatedDate datetime2 not null,
	UserId nvarchar(128)not null foreign key references AspNetUsers(Id)
)

CREATE TABLE BodyStyles (
	BodyStyleId int identity(1,1) not null primary key,
	BodyStyleName varchar(25) not null
)

CREATE TABLE Transmissions (
	TransmissionId char(1) not null primary key,
	TransmissionName varchar(25) not null
)

CREATE TABLE ExteriorColors (
	ExteriorColorId int identity(1,1) not null primary key,
	ExteriorColorName varchar(25) not null
)

CREATE TABLE VehicleTypes (
	VehicleTypeId char(1) not null primary key,
	VehicleTypeName varchar(25) not null
)

CREATE TABLE Interiors (
	InteriorId int identity(1,1) not null primary key,
	InteriorName varchar(25) not null
)

CREATE TABLE PurchaseTypes (
	PurchaseTypeId char(1) not null primary key,
	PurchaseTypeName varchar(50) not null,
)

CREATE TABLE States (
	StateId varchar(2) not null primary key,
	StateName varchar(15) not null
)

CREATE TABLE Vehicles (
	VehicleId int identity(1,1) not null primary key,
	MakeId int not null foreign key references Makes(MakeId),
	ModelId int not null foreign key references Models(ModelId),
	BodyStyleId int not null foreign key references BodyStyles(BodyStyleId),
	TransmissionId char(1) not null foreign key references Transmissions(TransmissionId),
	ExteriorColorId int not null foreign key references ExteriorColors(ExteriorColorId),
	InteriorId int not null foreign key references Interiors(InteriorId),
	VehicleTypeId char(1) not null foreign key references VehicleTypes(VehicleTypeId),
	VehicleYear decimal(4) not null,
	Milage decimal(9,2) not null,
	VinNumber varchar(17) not null,
	Price decimal(8,2) not null,
	MSRP decimal(8,2) not null,
	CreateDate datetime2 not null,
	ListingDescription varchar(500) not null,
	FeatureVehicle bit not null
)

CREATE TABLE VehicleImages (
	ImageId int identity(1,1) not null primary key,
	VehicleId int not null foreign key references Vehicles(VehicleId),
	ImageFileName varchar(50) not null
)

CREATE TABLE Purchases (
	PurchaseId int identity(1,1) not null primary key,
	VehicleId int not null foreign key references Vehicles(VehicleId),
	PurchaseName varchar(50) not null,
	Phone varchar(18) null,
	Email varchar(50) null,
	Street1 varchar(50) not null,
	Street2 varchar(50) null,
	City varchar(50) not null,
	StateId varchar(2) not null foreign key references States(StateId),
	Zipcode decimal(5) not null,
	PurchasePrice decimal(8,2) not null,
	PurchaseTypeId char(1) not null foreign key references PurchaseTypes(PurchaseTypeId),
	PurchaseDate datetime2 not null,
	UserId nvarchar(128)not null foreign key references AspNetUsers(Id)
)

CREATE TABLE Contacts (
	ContactId int identity(1,1) not null primary key,
	ContactName varchar(50) not null,
	ContactEmail varchar(50) null,
	ContactPhone varchar(18) null,
	ContactMessage varchar(500) null,
)

Create Table Specials(
	SpecialId int identity(1,1) not null primary key,
	SpecialName varchar(50) not null,
	SpecialDescription varchar(500) not null,
	SepcialImageFileName varchar(50) not null
)