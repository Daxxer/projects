﻿using GuildCars.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuildCars.Models.Queries;
using System.Data.SqlClient;
using System.Data;

namespace GuildCars.Data.ADO
{
    public class ReportsRepositoryADO : IReportsRepository
    {
        public string searchString()
        {
            string searchItem = "SELECT FirstName, LastName, SUM(PurchasePrice) AS TotalSales, COUNT(*) AS TotalVehicles " +
                                "FROM Purchases pur " +
                                    "INNER JOIN AspNetUsers apu ON pur.UserId = apu.Id " +
                                "WHERE 1 = 1 ";

            return searchItem;
        }

        public IEnumerable<SalesReportItem> SalesReport(SalesSearchParameters parameters)
        {
            List<SalesReportItem> searchItem = new List<SalesReportItem>();

            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                string query = searchString();

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;

                if (!string.IsNullOrEmpty(parameters.UserId))
                {
                    query += "AND UserId = @UserId ";
                    cmd.Parameters.AddWithValue("@UserId", parameters.UserId);
                }

                if (!string.IsNullOrEmpty(parameters.FromDate))
                {
                    query += "AND PurchaseDate >= @FromDate ";
                    cmd.Parameters.AddWithValue("@FromDate", parameters.FromDate);
                }

                if (!string.IsNullOrEmpty(parameters.ToDate))
                {
                    query += "AND PurchaseDate <= @ToDate ";
                    cmd.Parameters.AddWithValue("@ToDate", parameters.ToDate);
                }

                query += "GROUP BY FirstName, LastName " +
                         "ORDER BY TotalSales DESC ";

                cmd.CommandText = query;

                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        SalesReportItem currentRow = new SalesReportItem();
                        currentRow.FirstName = dr["FirstName"].ToString();
                        currentRow.LastName = dr["LastName"].ToString();
                        currentRow.TotalSales = (decimal)dr["TotalSales"];
                        currentRow.TotalVehicles = (int)dr["TotalVehicles"];

                        searchItem.Add(currentRow);
                    }
                }
            }

            return searchItem;
        }

        public IEnumerable<NewVehicleReportItem> NewVehicleReport()
        {
            List<NewVehicleReportItem> newVehicleReportItem = new List<NewVehicleReportItem>();

            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("NewInventoryReport", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        NewVehicleReportItem currentRow = new NewVehicleReportItem();
                        currentRow.Year = (decimal)dr["VehicleYear"];
                        currentRow.MakeName = dr["MakeName"].ToString();
                        currentRow.ModelName = dr["ModelName"].ToString();
                        currentRow.CountTotal = (int)dr["CountTotal"];
                        currentRow.StockValue = (decimal)dr["StockValue"];

                        newVehicleReportItem.Add(currentRow);
                    }
                }
            }

            return newVehicleReportItem;
        }

        public IEnumerable<UsedVehicleReportItem> UsedVehicleReport()
        {
            List<UsedVehicleReportItem> usedVehicleReportItem = new List<UsedVehicleReportItem>();

            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("UsedInventoryReport", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        UsedVehicleReportItem currentRow = new UsedVehicleReportItem();
                        currentRow.Year = (decimal)dr["VehicleYear"];
                        currentRow.MakeName = dr["MakeName"].ToString();
                        currentRow.ModelName = dr["ModelName"].ToString();
                        currentRow.CountTotal = (int)dr["CountTotal"];
                        currentRow.StockValue = (decimal)dr["StockValue"];

                        usedVehicleReportItem.Add(currentRow);
                    }
                }
            }

            return usedVehicleReportItem;
        }
    }
}
