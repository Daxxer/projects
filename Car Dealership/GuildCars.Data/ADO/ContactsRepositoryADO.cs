﻿using GuildCars.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuildCars.Models.Tables;
using System.Data.SqlClient;
using System.Data;

namespace GuildCars.Data.ADO
{
    public class ContactsRepositoryADO : IContactsRepository
    {
        public void AddContact(Contact contact)
        {
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("AddContacts", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter("@ContactId", SqlDbType.Int);
                param.Direction = ParameterDirection.Output;

                cmd.Parameters.Add(param);

                cmd.Parameters.AddWithValue("@ContactName", contact.ContactName);
                cmd.Parameters.AddWithValue("@ContactMessage", contact.ContactMessage);


                if (!string.IsNullOrEmpty(contact.ContactPhone))
                {
                    cmd.Parameters.AddWithValue("@ContactPhone", contact.ContactPhone);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@ContactPhone", DBNull.Value);
                }

                if (!string.IsNullOrEmpty(contact.ContactEmail))
                {
                    cmd.Parameters.AddWithValue("@ContactEmail", contact.ContactEmail);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@ContactEmail", DBNull.Value);
                }

                cn.Open();

                cmd.ExecuteNonQuery();

                contact.ContactId = (int)param.Value;
            }
        }
    }
}
