﻿using GuildCars.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuildCars.Models.Tables;
using System.Data.SqlClient;
using System.Data;

namespace GuildCars.Data.ADO
{
    public class PurchasesRepositoryADO : IPurchasesRepository
    {
        public void AddPurchase(Purchase purchase)
        {
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("AddPurchase", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter("@PurchaseId", SqlDbType.Int);
                param.Direction = ParameterDirection.Output;

                cmd.Parameters.Add(param);

                cmd.Parameters.AddWithValue("@VehicleId", purchase.VehicleId);
                cmd.Parameters.AddWithValue("@PurchaseName", purchase.PurchaseName);
                cmd.Parameters.AddWithValue("@Street1", purchase.Street1);
                cmd.Parameters.AddWithValue("@City", purchase.City);
                cmd.Parameters.AddWithValue("@StateId", purchase.StateId);
                cmd.Parameters.AddWithValue("@Zipcode", purchase.Zipcode);
                cmd.Parameters.AddWithValue("@PurchasePrice", purchase.PurchasePrice);
                cmd.Parameters.AddWithValue("@PurchaseTypeId", purchase.PurchaseTypeId);
                cmd.Parameters.AddWithValue("@PurchaseDate", purchase.PurchaseDate);
                cmd.Parameters.AddWithValue("@UserId", purchase.UserId);


                if (!string.IsNullOrEmpty(purchase.Phone))
                {
                    cmd.Parameters.AddWithValue("@Phone", purchase.Phone);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@Phone", DBNull.Value);
                }

                if (!string.IsNullOrEmpty(purchase.Email))
                {
                    cmd.Parameters.AddWithValue("@Email", purchase.Email);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@Email", DBNull.Value);
                }

                if (!string.IsNullOrEmpty(purchase.Street2))
                {
                    cmd.Parameters.AddWithValue("@Street2", purchase.Street2);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@Street2", DBNull.Value);
                }

                cn.Open();

                cmd.ExecuteNonQuery();

                purchase.PurchaseId = (int)param.Value;
            }
        }

        public IEnumerable<Purchase> GetAll()
        {
            List<Purchase> purchase = new List<Purchase>();

            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("PurchasesSelectAll", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Purchase currentRow = new Purchase();
                        currentRow.PurchaseId = (int)dr["PurchaseId"];
                        currentRow.VehicleId = (int)dr["VehicleId"];
                        currentRow.PurchaseName = dr["PurchaseName"].ToString();
                        currentRow.Phone = dr["Phone"].ToString();
                        currentRow.Email = dr["Email"].ToString();
                        currentRow.Street1 = dr["Street1"].ToString();
                        currentRow.Street2 = dr["Street2"].ToString();
                        currentRow.City = dr["City"].ToString();
                        currentRow.StateId = dr["StateId"].ToString();
                        currentRow.Zipcode = (decimal)dr["Zipcode"];
                        currentRow.PurchasePrice = (decimal)dr["PurchasePrice"];
                        currentRow.PurchaseTypeId = dr["PurchaseTypeId"].ToString();
                        currentRow.PurchaseDate = (DateTime)dr["PurchaseDate"];

                        purchase.Add(currentRow);
                    }
                }
            }

            return purchase;
        }
    }
}
