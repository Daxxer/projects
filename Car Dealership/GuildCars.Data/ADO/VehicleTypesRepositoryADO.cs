﻿using GuildCars.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuildCars.Models.Tables;
using System.Data.SqlClient;
using System.Data;

namespace GuildCars.Data.ADO
{
    public class VehicleTypesRepositoryADO : IVehicleTypesRepository
    {
        public IEnumerable<VehicleType> GetAll()
        {
            List<VehicleType> vehicleTypes = new List<VehicleType>();

            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("VehicleTypesSelectAll", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        VehicleType currentRow = new VehicleType();
                        currentRow.VehicleTypeId = dr["VehicleTypeId"].ToString();
                        currentRow.VehicleTypeName = dr["VehicleTypeName"].ToString();

                        vehicleTypes.Add(currentRow);
                    }
                }
            }

            return vehicleTypes;
        }
    }
}
