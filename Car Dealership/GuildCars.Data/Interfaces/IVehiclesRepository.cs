﻿using GuildCars.Models.Queries;
using GuildCars.Models.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuildCars.Data.Interfaces
{
    public interface IVehiclesRepository
    {
        IEnumerable<FeaturedVehicleItem> GetFeatured();
        VehicleDetailItem GetDetails(string vinNumber);
        EditVehicleItem GetVehicleToEdit(string vinNumber);
        void AddVehicle(Vehicle vehicle);
        void EditVehicle(EditVehicleItem vehicle);
        void DeleteVehicle(int vehicleId);
        IEnumerable<VehicleSearchResultItem> VehicleSearchNew(VehicleSearchParameters parameters);
        IEnumerable<VehicleSearchResultItem> VehicleSearchUsed(VehicleSearchParameters parameters);
        IEnumerable<VehicleSearchResultItem> VehicleSearchAll(VehicleSearchParameters parameters);
        void AddVehicleImage(Image image);

    }
}
