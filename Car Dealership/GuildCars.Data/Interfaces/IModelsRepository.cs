﻿using GuildCars.Models.Queries;
using GuildCars.Models.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuildCars.Data.Interfaces
{
    public interface IModelsRepository
    {
        IEnumerable<Model> GetAll();
        IEnumerable<Model> GetModelByMake(int makeId);
        void AddModel(Model model);
        IEnumerable<ModelReportItem> GetReport();
    }
}