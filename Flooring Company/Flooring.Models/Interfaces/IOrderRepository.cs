﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flooring.Models.Interfaces
{
    public interface IOrderRepository
    {
        void SetOrderDate(DateTime orderDate);
        void LoadOrders();
        bool DoesFileExist();
        Order RetriveOrder(int OrderNumber);
        List<Order> RetriveOrderList();
        void AddOrder(Order order);
        void SaveOrder(Order order);
        void RemoveOrder(Order order);
    }
}
