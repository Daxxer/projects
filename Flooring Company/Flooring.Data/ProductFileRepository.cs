﻿
using Flooring.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flooring.Data
{
    public class ProductFileRepository
    {

        private string _filePath = @"C:\Data\Flooring\Products.txt";

        private List<Products> productList;

        private void loadProductFile()
        {
            productList = new List<Products>();

            using (StreamReader sr = new StreamReader(_filePath))
            {
                sr.ReadLine();
                string line;

                while ((line = sr.ReadLine()) != null)
                {
                    Products newProductList = new Products();

                    string[] colums = line.Split(',');

                    newProductList.ProductType = colums[0];
                    newProductList.CostPerSquareFoot = decimal.Parse(colums[1]);
                    newProductList.LaborCostPerSquareFoot = decimal.Parse(colums[2]);

                    productList.Add(newProductList);
                }
            }
        }

        public List<Products> RetrieveProductList()
        {
            loadProductFile();

            return productList;
        }

        public Products RetriveProducts(string type)
        {
            loadProductFile();

            var product = productList.Where(p => p.ProductType == type);

            foreach (Products loadProduct in product)
            {
                return loadProduct;
            }

            return null;
        }
    }

}

