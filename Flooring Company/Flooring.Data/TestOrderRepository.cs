﻿using Flooring.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flooring.Models;

namespace Flooring.Data
{
    public class TestOrderRepository : IOrderRepository
    {

        private DateTime _orderDate;

        private static Order _order = new Order
        {
            OrderNumber = 1,
            CustomerName = "Wise",
            State = "OH",
            TaxRate = 6.25m,
            ProductType = "Wood",
            Area = 100m,
            CostPerSquareFoot = 5.15m,
            LaborCostPerSquareFoot = 4.75m,
            MaterialCost = 515m,
            LaborCost = 475m,
            Tax = 61.88m,
            Total = 1051.88m,
        };

        public void SetOrderDate(DateTime orderDate)
        {
            _orderDate = orderDate;
        }

        public Order RetriveOrder(int OrderNumber)
        {
            return _order;
        }

        public void LoadOrders()
        {
            throw new NotImplementedException();
        }

        public List<Order> RetriveOrderList()
        {
            List<Order> orderList = new List<Order>();

            orderList.Add(_order);

            return orderList;
        }

        public void AddOrder(Order order)
        {
            _order = order;
        }

        public void SaveOrder(Order order)
        {
            _order = order;
        }

        public void RemoveOrder(Order order)
        {
            throw new NotImplementedException();
        }

        public bool DoesFileExist()
        {
            throw new NotImplementedException();
        }
    }
}
