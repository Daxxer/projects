﻿using Flooring.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flooring.BLL
{
    public static class OrderManagerFactory
    {
        public static OrderManager Create()
        {
            string mode = ConfigurationManager.AppSettings["Mode"].ToString();

            switch (mode)
            {
                case "TestOrder":
                    return new OrderManager(new TestOrderRepository());
                case "FileOrder":
                    return new OrderManager(new FileOrderRepository());
                default:
                    throw new Exception("Mode Value in app config is not valid");
            }
        }
    }
}
