﻿using Flooring.Data;
using Flooring.Models;
using Flooring.Models.Interfaces;
using Flooring.Models.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flooring.BLL
{
    public class OrderManager
    {
        private IOrderRepository _orderRepository;

        public OrderManager(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }                  

        public bool LoadOrderRepositoryByDate( DateTime orderDate ) 
        {
            _orderRepository.SetOrderDate(orderDate);

            bool fileExists = _orderRepository.DoesFileExist();

            return fileExists;
        }

        public OrderLookupResponse LookupOrder(int OrderNumber)
        {
            OrderLookupResponse response = new OrderLookupResponse();

            response.Order = _orderRepository.RetriveOrder(OrderNumber);

            if (response.Order == null)
            { 
                response.Success = false;
            }
            else
            {
                response.Success = true;
            }

            return response;
        }

        public bool LookupProduct(string product)
        {
            bool isInformationValid = true;

            ProductFileRepository loadProductData = new ProductFileRepository();
            var productData = loadProductData.RetriveProducts(product);

            if (productData == null)
            {
                isInformationValid = false;
            }

            return isInformationValid;
        }        

        public bool LookupState(string state)
        {
            bool isInformationValid = true;

            TaxFileRepository loadTaxData = new TaxFileRepository();
            var taxData = loadTaxData.RetriveTaxData(state);

            if (taxData == null)
            {
                isInformationValid = false;
            }

            return isInformationValid;
        }               

        public List<Products> ProductList()
        {
            ProductFileRepository productRepo = new ProductFileRepository();

            List<Products> Products = productRepo.RetrieveProductList();

            return Products;
        }

        public OrderAddResponse AddOrder( 
            string customerName, string state, string productType, decimal Area, DateTime orderDate )
        {
            Order newOrder = new Order();
            AddOrderRules addRules = new AddOrderRules();
            OrderAddResponse response = new OrderAddResponse();

            newOrder.CustomerName = customerName;
            newOrder.State = state;
            newOrder.ProductType = productType;
            newOrder.Area = Area;

            response.Order = newOrder;

            response = addRules.AddOrder(response.Order, orderDate);

            return response;
        }

        public OrderEditResponse EditOrder(
            DateTime orderDate, int orderNumber, string customerName, string state, string productType, decimal Area)
        {
            OrderEditResponse response = new OrderEditResponse();

            _orderRepository.SetOrderDate(orderDate);

            response.Order = _orderRepository.RetriveOrder(orderNumber);

            if (response.Order == null)
            {
                response.Success = false;
                response.Message = $"{orderNumber} is not a valid order.";
                return response;
            }
            else
            {
                response.Success = true;
            }

            EditOrderRules editRule = new EditOrderRules();

            response = editRule.EditOrder(response.Order, customerName, state, productType, Area);

            return response;
        }

        public void SaveOrder(Order order, DateTime orderDate)
        {
            _orderRepository.SetOrderDate(orderDate);
            _orderRepository.SaveOrder(order);
        }

        public void SaveNewOrder(Order order, DateTime orderDate)
        {
            _orderRepository.SetOrderDate(orderDate);
            _orderRepository.AddOrder(order);
        }

        public void RemoveRequsetedOrder(Order order, DateTime orderDate)
        {
            _orderRepository.SetOrderDate(orderDate);
            _orderRepository.RemoveOrder(order);
        }

        public OrderRemoveResponse RemoveOrder(DateTime orderDate, int orderNumber)
        {
            OrderRemoveResponse response = new OrderRemoveResponse();

            _orderRepository.SetOrderDate(orderDate);

            response.Order = _orderRepository.RetriveOrder(orderNumber);

            if (response.Order == null)
            {
                response.Success = false;
                response.Message = $"{orderNumber} is not a valid order.";
                return response;
            }
            else
            {
                response.Success = true;
            }

            //RemoveOrderRules removeRule = new RemoveOrderRules();

            //response = removeRule.RemoveOrder(orderDate, response.Order);

            if (response.Success)
            {
               
            }

            return response;
        }
    }
}
