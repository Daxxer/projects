﻿using Flooring.Data;
using Flooring.Models;
using Flooring.Models.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flooring.BLL
{
    public class EditOrderRules
    {
        public OrderEditResponse EditOrder(Order order, string customerName, string state, string productType, decimal area)
        {
            OrderEditResponse response = new OrderEditResponse();

            response.Order = order;

            if (customerName != "")
            {
                response.Order.CustomerName = customerName;
            }

            if (state != "")
            {
                response.Order.State = state;
            }

            TaxFileRepository loadTaxData = new TaxFileRepository();
            var taxData = loadTaxData.RetriveTaxData(response.Order.State);

            if (productType != "")
            {
                response.Order.ProductType = productType;
            }

            ProductFileRepository loadProductData = new ProductFileRepository();
            var productData = loadProductData.RetriveProducts(response.Order.ProductType);

            if (area != 0)
            {
                response.Order.Area = area;
            }

            if (response.Order.Area < 100)
            {
                response.Success = false;
                Console.WriteLine("Product Area must be a minimum of 100 sq ft.");
                return response;
            }

            if (taxData == null)
            {
                response.Success = false;
                response.Message = "We can not sell in that state.";
                return response;
            }

            if (productData == null)
            {
                response.Success = false;
                response.Message = "The product type entered is not somthing we sell.";
                return response;
            }

            response.Success = true;
            response.Order.TaxRate = taxData.TaxRate;
            response.Order.CostPerSquareFoot = productData.CostPerSquareFoot;
            response.Order.LaborCostPerSquareFoot = productData.LaborCostPerSquareFoot;
            response.Order.MaterialCost = response.Order.Area * response.Order.CostPerSquareFoot;
            response.Order.LaborCost = response.Order.Area * response.Order.LaborCostPerSquareFoot;
            response.Order.Tax = (response.Order.MaterialCost + response.Order.LaborCost) * (response.Order.TaxRate / 100);
            response.Order.Total = response.Order.MaterialCost + response.Order.LaborCost + response.Order.Tax;

            return response;
        }
    }
}
