﻿using Flooring.UI.Workflows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flooring.UI
{
    public static class Menu
    {
        public static void Start()
        {          
            while (ValidateDB.validateFiles())
            {
                Console.Clear();
                Console.WriteLine("*************************************************************************************");
                Console.WriteLine("* Flooring Program");
                Console.WriteLine("* ");
                Console.WriteLine("* 1. Display Orders");
                Console.WriteLine("* 2. Add an Order");
                Console.WriteLine("* 3. Edit an Order");
                Console.WriteLine("* 4. Remove an Order");
                Console.WriteLine("* 5. Quit");
                Console.WriteLine("* ");
                Console.WriteLine("*************************************************************************************");
                Console.WriteLine();
                Console.Write("\nEnter selection: ");
                string userinput = Console.ReadLine().ToUpper();

                switch (userinput)
                {
                    case "1":
                        OrderLookupWorkflow lookupWorkflow = new OrderLookupWorkflow();
                        lookupWorkflow.Execute();
                        break;
                    case "2":
                        AddOrderWorkflow addOrderWorkflow = new AddOrderWorkflow();
                        addOrderWorkflow.Execute();
                        break;
                    case "3":
                        EditOrderWorkflow editOrderWorkFlow = new EditOrderWorkflow();
                        editOrderWorkFlow.Execute();
                        break;
                    case "4":
                        RemoveOrderWorkflow removeOrderWorkFlow = new RemoveOrderWorkflow();
                        removeOrderWorkFlow.Execute();
                        break;
                    case "5":
                        return;
                    default:
                        break;
                }
            }
        }
    }
}
