﻿using Flooring.BLL;
using Flooring.Models.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flooring.UI.Workflows
{
    public class RemoveOrderWorkflow
    {
        public void Execute()
        {
            OrderManager manager = OrderManagerFactory.Create();

            Console.Clear();
            Console.WriteLine("Remove an order");
            Console.WriteLine();
            Console.WriteLine("--------------------------");

            bool isInformationValid = true;
            string dateInput;
            DateTime orderDate = new DateTime();
            bool keepLooping = true;

            while (keepLooping)
            {
                Console.Write("Enter a Date (Q to quit): ");
                dateInput = Console.ReadLine();

                if (dateInput == "q" || dateInput == "Q")
                {
                    isInformationValid = false;
                    keepLooping = false;
                    break;
                }

                if (ConsoleIO.CheckDate(dateInput, manager))
                {
                    orderDate = DateTime.Parse(dateInput);
                    keepLooping = false;
                    isInformationValid = true;
                }

            }

            int inputForOrderNumber = 0;
            string stringInputForOrderNumber = null;

            OrderLookupResponse retrieveOrder = new OrderLookupResponse();

            if (isInformationValid)
            {
                keepLooping = true;
            }

            while (keepLooping)
            {
                if (isInformationValid)
                {
                    Console.Write("Enter Order Number (Q to quit): ");
                    stringInputForOrderNumber = Console.ReadLine();

                    if (stringInputForOrderNumber == "q" || stringInputForOrderNumber == "Q")
                    {
                        isInformationValid = false;
                        keepLooping = false;
                        break;
                    }

                    if (ConsoleIO.CheckOrderNumber(stringInputForOrderNumber, manager))
                    {
                        keepLooping = false;
                        isInformationValid = true;
                    }
                }
            }

            if (isInformationValid)
            {
                inputForOrderNumber = int.Parse(stringInputForOrderNumber);
                retrieveOrder = manager.LookupOrder(inputForOrderNumber);

                ConsoleIO.DisplayOrderDetails(retrieveOrder.Order, orderDate);

                Console.Write("Is this the order you want to Remove?");
                string yesOrNo = ConsoleIO.GetYesNoAnswerFromUser();

                if (yesOrNo != "Y")
                {
                    isInformationValid = false;
                }
            }

            
            if (isInformationValid)
            {
                OrderRemoveResponse response = manager.RemoveOrder(orderDate, inputForOrderNumber);

                if (response.Success)
                {
                    manager.RemoveRequsetedOrder(response.Order, orderDate);
                    Console.WriteLine();
                    Console.WriteLine($"Order Number {response.Order.OrderNumber} that was for customer name {response.Order.CustomerName} was removed");
                    Console.WriteLine();
                }

                else
                {
                    Console.WriteLine("An error occurred: ");
                    Console.WriteLine(response.Message);
                }
            }            

            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }
    }
}
