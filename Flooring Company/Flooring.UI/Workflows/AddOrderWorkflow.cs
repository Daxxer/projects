﻿using Flooring.BLL;
using Flooring.Models;
using Flooring.Models.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flooring.UI.Workflows
{
    public class AddOrderWorkflow
    {
        public void Execute()
        {
            OrderManager manager = OrderManagerFactory.Create();

            bool isInformationValid = true;

            Console.Clear();
            Console.WriteLine("Add an Order");
            Console.WriteLine("--------------------------");

            string dateInput;
            DateTime orderDate = new DateTime();
            bool keepLooping = true;

            while (keepLooping)
            {
                Console.Write("Enter a Date (Q to quit): ");
                dateInput = Console.ReadLine();

                if (dateInput == "q" || dateInput == "Q")
                {
                    isInformationValid = false;
                    keepLooping = false;
                    break;
                }

                if (ConsoleIO.IsDateInPast(dateInput))
                {
                    orderDate = DateTime.Parse(dateInput);
                    keepLooping = false;
                    isInformationValid = true;
                }
            }

            OrderAddResponse response = new OrderAddResponse();

            if (isInformationValid)
            {
                string inputForCustomerName = ConsoleIO.GetValidCustomerNameFromUser();
                if (inputForCustomerName == "")
                {
                    Console.WriteLine("The customer name can not be empty.");
                    inputForCustomerName = ConsoleIO.GetValidCustomerNameFromUser();
                }

                string inputForState = ConsoleIO.GetValidStateFromUser(manager);
                if (inputForState == "")
                {
                    Console.WriteLine("The state can not be empty.");
                    inputForState = ConsoleIO.GetValidStateFromUser(manager);
                }

                string inputForProductType = ConsoleIO.GetValidProductTypeFromUser(manager);
                if (inputForProductType == "")
                {
                    Console.WriteLine("The product type can not be empty.");
                    inputForProductType = ConsoleIO.GetValidProductTypeFromUser(manager);
                }

                decimal inputForArea = ConsoleIO.GetAreaFromUser();
                if (inputForArea == 0)
                {
                    Console.WriteLine("Area can not be empty.");
                }

                response = manager.AddOrder(
                    inputForCustomerName, inputForState, inputForProductType, inputForArea, orderDate);

                string finalYesNoFromUser = null;

                if (response.Success)
                {
                    Console.WriteLine();
                    ConsoleIO.DisplayOrderDetails(response.Order, orderDate);
                    Console.Write("Do you want to save this order?");
                    finalYesNoFromUser = ConsoleIO.GetYesNoAnswerFromUser();
                }

                if (finalYesNoFromUser == "Y")
                {
                    manager.SaveNewOrder(response.Order, orderDate);
                    Console.WriteLine();
                    Console.WriteLine("The order has been saved!");
                    Console.WriteLine();
                }

                else
                {
                    Console.WriteLine("An error occurred: ");
                    Console.WriteLine(response.Message);
                }
            }
                       
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }
    }
}
