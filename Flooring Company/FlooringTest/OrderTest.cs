﻿using Flooring.BLL;
using Flooring.Models;
using Flooring.Models.Responses;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flooring.UI
{
    [TestFixture]
    public class OrderTest
    {
        private const string _testOrderRepo = @"C:\Data\Flooring\Test Data\Orders_06012013.txt";
        private const string _testOrderRepoOriginal = @"C:\Data\Flooring\Test Data\Orders_06012013_seed.txt";
        private const string _testProductRepo = @"C:\Data\Flooring\Test Data\Products.txt";
        private const string _testTaxRepo = @"C:\Data\Flooring\Test Data\Taxes.txt";

        [SetUp]
        public void OrderSetup()
        {
            if (File.Exists(_testOrderRepo))
            {
                File.Delete(_testOrderRepo);
            }

            File.Copy(_testOrderRepoOriginal, _testOrderRepo);
        }

        [Test]
        public void CanLoadOrderData()
        {
            OrderManager manager = OrderManagerFactory.Create();
            OrderLookupResponse response = manager.LookupOrder(1);

            Assert.IsNotNull(response.Order);
            Assert.IsTrue(response.Success);
            Assert.AreEqual(1, response.Order.OrderNumber);

        }

        [TestCase("Customer1", "OH", "Wood", 120, true)]
        [TestCase("Customer2", "PA", "Tile", 100, true)]
        [TestCase("Customer3", "FL", "Wood", 120, false)]
        [TestCase("Customer4", "OH", "Food", 120, false)]
        [Test]
        public void CanAddOrder(string customerName, string state, string productType, decimal area ,bool expectedResult)
        {
            DateTime orderDate = DateTime.Now;

            OrderManager manager = OrderManagerFactory.Create();
            OrderAddResponse response = manager.AddOrder(customerName, state, productType, area, orderDate);
            expectedResult = response.Success;

            Assert.AreEqual(expectedResult, response.Success);
        }

        [Test]
        public void CanDeleteOrder()
        {
            DateTime orderDate = DateTime.Now;

            OrderManager manager = OrderManagerFactory.Create();
            OrderRemoveResponse response = manager.RemoveOrder(orderDate, 1);

            Assert.IsTrue(response.Success);
        }

        [Test]
        public void CanEditOrderToFile()
        {
            DateTime orderDate = DateTime.Now;
            int orderNumber = 1;
            string customerName = "Mr.Wise";
            string state = "OH";
            string productType = "Tile";
            decimal Area = 300;

            OrderManager manager = OrderManagerFactory.Create();
            OrderEditResponse response = manager.EditOrder(orderDate, orderNumber, customerName, state, productType, Area);

            Assert.IsTrue(response.Success);
        }
    }
}
