$(document).ready(function () {
		loadDvdList();

		$('#search-dvd-button').click(function (event){
			loadDvdSearch();
		});

		$('#dvd-details-close-button').click(function (event){
			hideDvdDetailsDiv();
		});

		$('#create-dvd-button').click(function (event){
			showCreateDvdDiv();
		});

		$('#add-submit-button').click(function (event){
			createDvd();
		});

		$('#edit-submit-button').click(function (event){
			editDvd();
		});

		$('#edit-cancel-button').click(function (event){
			hidEditDvdDiv();
		});

});

var mainMenuDiv = $('#main-Menu-Page');
var addDvdDiv = $('#add-Dvd-Page');
var editDvdDiv = $('#edit-Dvd-Page');
var dvdDetailsDiv = $('#dvd-Details-Page');
var dvdRow = $('#dvd-List');

function clearErrorMessages() {
	$('#main-Menu-Errors').empty();
	$('#add-error-messages').empty();	
	$('#edit-error-messages').empty();
}

function loadDvdList() {
	dvdRow.empty();

	$.ajax({
		type:'GET',
		url: 'http://localhost:55826/dvds',

		success: function(dvdArray) {
			$.each(dvdArray, function(index, dvd){
				var dvdId = dvd.dvdId;
				var title = dvd.title;
				var year = dvd.releaseYear;
				var director = dvd.director;
				var rating = dvd.rating;
				var notes = dvd.notes;

				var row = '<tr>';
					row += '<td><a onclick="showDvdDetailsDiv('+ dvdId +')">' + title + '</a></td>';
					row += '<td>' + year + '</td>';
					row += '<td>' + director + '</td>';
					row += '<td>' + rating + '</td>';
					row += '<td><a onclick="showEditDvdDiv('+ dvdId +')">Edit</a> |' +
								'<a onclick="deleteDvdPromt('+ dvdId +')">Delete</a></td>';
					row += '</tr>';

				dvdRow.append(row);
			});
		},
		error: function(){
			$('#main-Menu-Errors')
				.append($('<li>')
				.attr({class: 'list-group-item list-group-item-danger'})
				.text('Error calling web service.'));
		}
	});
}

function loadDvdSearch() {

	var validationError = errorChecking($('#main-menu-form').find('.main-menu-required'), $('#main-Menu-Errors'));

	if (validationError) {		
		return false;
	};

	dvdRow.empty();

	$.ajax({
		type: 'GET',
		url: 'http://localhost:55826/dvds/' + $('#select-Catagory').val() + '/' +$('#search-term-input').val(),

		success: function(dvdArray) {
			$.each(dvdArray, function(index, dvd){
				var dvdId = dvd.dvdId;
				var title = dvd.title;
				var year = dvd.releaseYear;
				var director = dvd.director;
				var rating = dvd.rating;
				var notes = dvd.notes;

				var row = '<tr>';
					row += '<td><a onclick="showDvdDetailsDiv('+ dvdId +')">' + title + '</a></td>';
					row += '<td>' + year + '</td>';
					row += '<td>' + director + '</td>';
					row += '<td>' + rating + '</td>';
					row += '<td><a onclick="showEditDvdDiv('+ dvdId +')">Edit</a> |' +
								'<a onclick="deleteDvdPromt('+ dvdId +')">Delete</a></td>';
					row += '</tr>';

				dvdRow.append(row);
			});
		},
		error: function() {
			$('#main-Menu-Errors')
				.append($('<li>')
				.attr({class: 'list-group-item list-group-item-danger'})
				.text('Error calling web service.'));
		}
	});
}

function showDvdDetailsDiv(dvdId) {
	mainMenuDiv.hide();
	addDvdDiv.hide();
	editDvdDiv.hide();
	dvdDetailsDiv.show();

	$.ajax({
		type: 'GET',
		url: 'http://localhost:55826/dvd/' + dvdId,

		success: function(responce) {
			
			$('#dvd-details-title').append(responce.title);

			var title = responce.title;
			var year = responce.releaseYear;
			var director = responce.director;
			var rating = responce.rating;
			var notes = responce.notes;

			var row1 = '<tr>';
				row1 += '<td>Release Year:</td>';
				row1 +=	'<td>' + year +'</td>';
				row1 += '</tr>';

			var row2 = '<tr>';
				row2 += '<td>Director:</td>';
				row2 += '<td>' + director + '</td>';
				row2 += '</tr>';

			var row3 = '<tr>';
				row3 += '<td>Rating:</td>';
				row3 += '<td>' + rating + '</td>';
				row3 += '</tr>';

			var row4 = '<tr>';
				row4 += '<td>Notes:</td>';
				row4 += '<td>' + notes + '</td>';
				row4 += '</tr>';

			$('#dvd-details-row').append(row1);
			$('#dvd-details-row').append(row2);
			$('#dvd-details-row').append(row3);
			$('#dvd-details-row').append(row4);			
		},
		error: function() {

		}
	});
}

function hideDvdDetailsDiv() {
	mainMenuDiv.show();
	addDvdDiv.hide();
	editDvdDiv.hide();
	dvdDetailsDiv.hide();

	clearErrorMessages();

	$('#dvd-details-title').empty();
	$('#dvd-details-row').empty();
}

function showCreateDvdDiv() {
	mainMenuDiv.hide();
	addDvdDiv.show();
	editDvdDiv.hide();
	dvdDetailsDiv.hide();
}

function createDvd() {

	var validationError = errorChecking($('#add-form').find('input'), $('#add-error-messages'));
	var validateYear = checkYear($('#add-dvd-year').val(), $('#add-error-messages'));

	if (validationError) {		
		return false;
	}

	if (validateYear) {		
		return false;
	}

	$.ajax({
		type: 'POST',
		url: 'http://localhost:55826/dvd',
		data: JSON.stringify({
			title: $('#add-dvd-title').val(),
			releaseYear: $('#add-dvd-year').val(),
			director: $('#add-dvd-director').val(),
			rating: $('#add-dvd-rating').val(),
			notes: $('#add-dvd-notes').val(),
		}),
		headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
		'dataType': 'json',

		success: function() {
			clearErrorMessages()
			hideCreateDvdDiv();
		},

		error: function() {

		}
	});

}

function hideCreateDvdDiv() {
	mainMenuDiv.show();
	addDvdDiv.hide();
	editDvdDiv.hide();
	dvdDetailsDiv.hide();

	clearErrorMessages();
	loadDvdList();

	$('#add-dvd-title').val('');
	$('#add-dvd-year').val('');
	$('#add-dvd-director').val('');
	$('#add-dvd-notes').val('');
	$('#add-dvd-rating').val('G');
}

function showEditDvdDiv(dvdId) {
	mainMenuDiv.hide();
	addDvdDiv.hide();
	editDvdDiv.show();
	dvdDetailsDiv.hide();

	$.ajax({
		type:'GET',
		url: 'http://localhost:55826/dvd/' + dvdId,

		success: function(responce){

			$('#edit-dvd-page-title').append('Edit Dvd: ' + responce.title);

			$('#edit-dvd-id').val(responce.dvdId);
			$('#edit-dvd-title').val(responce.title);
			$('#edit-dvd-year').val(responce.releaseYear);
			$('#edit-dvd-director').val(responce.director);
			$('#edit-dvd-rating').val(responce.rating);
			$('#edit-dvd-notes').val(responce.notes);
		},

		error: function() {
			$('#edit-error-messages')
				.append($('<li>')
				.attr({class: 'list-group-item list-group-item-danger'})
				.text('Error calling web service.'));
		}
	});
}

function editDvd() {
	var validationError = errorChecking($('#edit-form').find('input'), $('#edit-error-messages'));
	var validateYear = checkYear($('#edit-dvd-year').val(), $('#edit-error-messages'));

	if (validationError) {		
		return false;
	}

	if (validateYear) {		
		return false;
	}

	$.ajax({
		type: 'PUT',
		url: 'http://localhost:55826/dvd/'+ $('#edit-dvd-id').val(),
		data: JSON.stringify({
			dvdId: $('#edit-dvd-id').val(),
			title: $('#edit-dvd-title').val(),
			releaseYear: $('#edit-dvd-year').val(),
			director: $('#edit-dvd-director').val(),
			rating: $('#edit-dvd-rating').val(),
			notes: $('#edit-dvd-notes').val()
		}),
		headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
		},
		'dataType': 'json',

		success: function() {
			clearErrorMessages();
			hidEditDvdDiv();
		},

		error: function() {
			$('#edit-error-messages')
				.append($('<li>')
				.attr({class: 'list-group-item list-group-item-danger'})
				.text('Error editing dvd.'));
		}
	});
}

function hidEditDvdDiv() {
	mainMenuDiv.show();
	addDvdDiv.hide();
	editDvdDiv.hide();
	dvdDetailsDiv.hide();

	clearErrorMessages();
	loadDvdList();

	$('#edit-dvd-title').val('');
	$('#edit-dvd-year').val('');
	$('#edit-dvd-director').val('');
	$('#edit-dvd-notes').val('');
	$('#edit-dvd-rating').val('G');
}

function deleteDvdPromt(dvdId) {
	var popup = confirm("Are you sure you want to delete this Dvd from your collection?");

	if (popup == true) {
		$.ajax({
			type: 'DELETE',
			url: 'http://localhost:55826/dvd/' + dvdId,

			success: function() {
				loadDvdList();
			}
		});
	};
}

function errorChecking(input, field){
	clearErrorMessages();

	var errorMessages = [];

	input.each(function(){
		if (!this.validity.valid) {

			var errorField = $('label[for=' + this.id + ']').text();
			errorMessages.push(errorField + ' ' + this.validationMessage);
		}		
	});

	if (errorMessages.length > 0 ) {
		$.each(errorMessages, function(index, message){
			field.append($('<li>').attr({class: 'list-group-item list-group-item-danger'}).text(message));
		
			console.log(errorMessages);
			console.log(message);
		});		

		return true;
	}
	else {
		return false;
	}
}

function checkYear(input, field) {

	if (input.length != 4)  {

		field.append($('<li>')
			.attr({class: 'list-group-item list-group-item-danger'})
			.text('Please enter 4 digits for the year.'));
		return true;
	}

	else {
		return false;
	}
}