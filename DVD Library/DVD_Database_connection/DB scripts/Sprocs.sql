Use Dvd
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'LoadDvdList')
      DROP PROCEDURE LoadDvdList
GO

CREATE PROCEDURE LoadDvdList AS
BEGIN
	SELECT DvdId, Title, ReleaseYear, Director, Rating, Notes
	FROM DvdDetails
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'LoadDvdListByTitle')
      DROP PROCEDURE LoadDvdListByTitle
GO

CREATE PROCEDURE LoadDvdListByTitle (
	@Title nvarchar(50)
)AS
BEGIN
	SELECT DvdId, Title, ReleaseYear, Director, Rating, Notes
	FROM DvdDetails
	WHERE Title = @Title
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'LoadDvdListByReleaseYear')
      DROP PROCEDURE LoadDvdListByReleaseYear
GO

CREATE PROCEDURE LoadDvdListByReleaseYear (
	@ReleaseYear nvarchar(50)
)AS
BEGIN
	SELECT DvdId, Title, ReleaseYear, Director, Rating, Notes
	FROM DvdDetails
	WHERE ReleaseYear = @ReleaseYear
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'LoadDvdListByDirector')
      DROP PROCEDURE LoadDvdListByDirector
GO

CREATE PROCEDURE LoadDvdListByDirector (
	@Director nvarchar(50)
)AS
BEGIN
	SELECT DvdId, Title, ReleaseYear, Director, Rating, Notes
	FROM DvdDetails
	WHERE Director = @Director
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'LoadDvdListByRating')
      DROP PROCEDURE LoadDvdListByRating
GO

CREATE PROCEDURE LoadDvdListByRating (
	@Rating nvarchar(5)
)AS
BEGIN
	SELECT DvdId, Title, ReleaseYear, Director, Rating, Notes
	FROM DvdDetails
	WHERE Rating = @Rating
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'GetDvdDetails')
      DROP PROCEDURE GetDvdDetails
GO

CREATE PROCEDURE GetDvdDetails (
	@DvdId int
)
AS

BEGIN
	SELECT DvdId, Title, ReleaseYear, Director, Rating, Notes
	FROM DvdDetails
	WHERE Dvdid = @DvdId
END
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'DvdInsert')
      DROP PROCEDURE DvdInsert
GO

CREATE PROCEDURE DvdInsert (
	@DvdId int output,
	@Title nvarchar(50),
	@ReleaseYear int,
	@Director nvarchar(50),
	@Rating nvarchar(5),
	@Notes nvarchar(100)

) AS
BEGIN
	INSERT INTO DvdDetails (Title, ReleaseYear, Director, Rating, Notes)
	VALUES (@Title, @ReleaseYear, @Director, @Rating, @Notes);

	SET @DvdId = SCOPE_IDENTITY();
END

GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'DvdUpdate')
      DROP PROCEDURE DvdUpdate
GO

CREATE PROCEDURE DvdUpdate (
	@DvdId int,
	@Title nvarchar(50),
	@ReleaseYear int,
	@Director nvarchar(50),
	@Rating nvarchar(5),
	@Notes nvarchar(100)
) AS
BEGIN
	UPDATE DvdDetails SET 
		Title = @Title, 
		ReleaseYear = @ReleaseYear, 
		Director = @Director, 
		Rating = @Rating, 
		Notes = @Notes
	WHERE DvdId = @DvdId
END

GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.ROUTINES
   WHERE ROUTINE_NAME = 'DvdDelete')
      DROP PROCEDURE DvdDelete
GO

CREATE PROCEDURE DvdDelete (
	@DvdId int
) AS
BEGIN
	BEGIN TRANSACTION

	DELETE FROM DvdDetails WHERE DvdId = @DvdId;

	COMMIT TRANSACTION
END
GO