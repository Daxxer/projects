﻿using DVD_Database_connection.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace DVD_Database_connection.Models
{
    public class DvdRepository
    {
       public static DVD GetById(int dvdId)
        {
            DVD dvd = null;

            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("GetDvdDetails", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@DvdId", dvdId);

                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.Read())
                    {
                        dvd = new DVD();
                        dvd.DvdId = (int)dr["DvdId"];
                        dvd.Title = dr["Title"].ToString();
                        dvd.ReleaseYear = (int)dr["ReleaseYear"];
                        dvd.Director = dr["Director"].ToString();
                        dvd.Rating = dr["Rating"].ToString();

                        if (dr["Notes"] != DBNull.Value)
                        {
                            dvd.Notes = dr["Notes"].ToString();
                        }                        
                    }
                }
            }

            return dvd;
        }

        public static IEnumerable<DVD> SearchByTitle(string title)
        {
            List<DVD> dvd = new List<DVD>();

            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("LoadDvdListByTitle", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Title", title);

                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        DVD row = new DVD();

                        row.DvdId = (int)dr["DvdId"];
                        row.Title = dr["Title"].ToString();
                        row.Director = dr["Director"].ToString();
                        row.ReleaseYear = (int)dr["ReleaseYear"];
                        row.Rating = dr["Rating"].ToString();

                        if (dr["Notes"] != DBNull.Value)
                        {
                            row.Notes = dr["Notes"].ToString();
                        }

                        dvd.Add(row);
                    }
                }
            }

            return dvd;
        }

        public static IEnumerable<DVD> SearchByReleaseYear(int releaseYear)
        {
            List<DVD> dvd = new List<DVD>();

            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("LoadDvdListByReleaseYear", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@ReleaseYear", releaseYear);

                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        DVD row = new DVD();

                        row.DvdId = (int)dr["DvdId"];
                        row.Title = dr["Title"].ToString();
                        row.Director = dr["Director"].ToString();
                        row.ReleaseYear = (int)dr["ReleaseYear"];
                        row.Rating = dr["Rating"].ToString();

                        if (dr["Notes"] != DBNull.Value)
                        {
                            row.Notes = dr["Notes"].ToString();
                        }

                        dvd.Add(row);
                    }
                }
            }

            return dvd;
        }

        public static IEnumerable<DVD> SearchByDirector(string director)
        {
            List<DVD> dvd = new List<DVD>();

            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("LoadDvdListByDirector", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Director", director);

                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        DVD row = new DVD();

                        row.DvdId = (int)dr["DvdId"];
                        row.Title = dr["Title"].ToString();
                        row.Director = dr["Director"].ToString();
                        row.ReleaseYear = (int)dr["ReleaseYear"];
                        row.Rating = dr["Rating"].ToString();

                        if (dr["Notes"] != DBNull.Value)
                        {
                            row.Notes = dr["Notes"].ToString();
                        }

                        dvd.Add(row);
                    }
                }
            }

            return dvd;
        }

        public static IEnumerable<DVD> SearchByRating(string rating)
        {
            List<DVD> dvd = new List<DVD>();

            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("LoadDvdListByRating", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Rating", rating);

                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        DVD row = new DVD();

                        row.DvdId = (int)dr["DvdId"];
                        row.Title = dr["Title"].ToString();
                        row.Director = dr["Director"].ToString();
                        row.ReleaseYear = (int)dr["ReleaseYear"];
                        row.Rating = dr["Rating"].ToString();

                        if (dr["Notes"] != DBNull.Value)
                        {
                            row.Notes = dr["Notes"].ToString();
                        }

                        dvd.Add(row);
                    }
                }
            }

            return dvd;
        }

        public static IEnumerable<DVD> LoadDvdList()
        {
            List<DVD> dvd = new List<DVD>();

            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("LoadDvdList", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        DVD row = new DVD();

                        row.DvdId = (int)dr["DvdId"];
                        row.Title = dr["Title"].ToString();
                        row.Director = dr["Director"].ToString();
                        row.ReleaseYear = (int)dr["ReleaseYear"];
                        row.Rating = dr["Rating"].ToString();

                        if (dr["Notes"] != DBNull.Value)
                        {
                            row.Notes = dr["Notes"].ToString();
                        }

                        dvd.Add(row);
                    }
                }
            }

            return dvd;
        }

        public static void Insert(DVD dvd)
        {
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("DvdInsert", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter("@DvdId", SqlDbType.Int);
                param.Direction = ParameterDirection.Output;

                cmd.Parameters.Add(param);

                cmd.Parameters.AddWithValue("@Title", dvd.Title);
                cmd.Parameters.AddWithValue("@ReleaseYear", dvd.ReleaseYear);
                cmd.Parameters.AddWithValue("@Director", dvd.Director);
                cmd.Parameters.AddWithValue("@Rating", dvd.Rating);
                cmd.Parameters.AddWithValue("@Notes", dvd.Notes);

                cn.Open();

                cmd.ExecuteNonQuery();

                dvd.DvdId = (int)param.Value;
            }
        }

        public static void Update(DVD dvd)
        {
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("DvdUpdate", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@DvdId", dvd.DvdId);
                cmd.Parameters.AddWithValue("@Title", dvd.Title);
                cmd.Parameters.AddWithValue("@ReleaseYear", dvd.ReleaseYear);
                cmd.Parameters.AddWithValue("@Director", dvd.Director);
                cmd.Parameters.AddWithValue("@Rating", dvd.Rating);
                cmd.Parameters.AddWithValue("@Notes", dvd.Notes);

                cn.Open();

                cmd.ExecuteNonQuery();
            }
        }

        public static void Delete(int dvdId)
        {
            using (var cn = new SqlConnection(Settings.GetConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("DvdDelete", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@DvdId", dvdId);

                cn.Open();

                cmd.ExecuteNonQuery();
            }
        }
    }
}