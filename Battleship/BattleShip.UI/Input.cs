﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BattleShip.BLL.Requests;

namespace BattleShip.UI
{
    class Input
    {

        public string GetPlayerName(int playerNumber)
        {
            Console.Clear();
            Console.Write($"Enter the name of player {playerNumber}: ");
            string inputedName = Console.ReadLine();

            while (inputedName.Length < 1)
            {
                Console.Clear();
                Console.WriteLine("You must input something for a name.");
                Console.Write($"Enter the name of player {playerNumber}: ");
                inputedName = Console.ReadLine();
            }

            return inputedName;
        }

        public Coordinate GetCoordinate()
        {

            string getX;
            string getY;
            int x = 0;
            int y = 0;

            Console.Write("Input your coordinates (example B3): ");
            string input = Console.ReadLine();

            if ((input.Length < 2) || (input.Length > 3))
            {
                Console.WriteLine("That was not a valid board coordinate.");
                GetCoordinate();
            }

            else
            {
                getX = input.Remove(1);
                getY = input.Substring(1, input.Length - 1);
                int.TryParse(getY, out y);

                switch (getX)
                {
                    case "A":
                        x = 1;
                        break;
                    case "a":
                        x = 1;
                        break;
                    case "B":
                        x = 2;
                        break;
                    case "b":
                        x = 2;
                        break;
                    case "C":
                        x = 3;
                        break;
                    case "c":
                        x = 3;
                        break;
                    case "D":
                        x = 4;
                        break;
                    case "d":
                        x = 4;
                        break;
                    case "E":
                        x = 5;
                        break;
                    case "e":
                        x = 5;
                        break;
                    case "F":
                        x = 6;
                        break;
                    case "f":
                        x = 6;
                        break;
                    case "G":
                        x = 7;
                        break;
                    case "g":
                        x = 7;
                        break;
                    case "H":
                        x = 8;
                        break;
                    case "h":
                        x = 8;
                        break;
                    case "I":
                        x = 9;
                        break;
                    case "i":
                        x = 9;
                        break;
                    case "J":
                        x = 10;
                        break;
                    case "j":
                        x = 10;
                        break;
                    default:
                        break;
                }
            }
            
            return new Coordinate(x, y);

        }

        public ShipDirection GetShipDirection()
        {
            while (true)
            {
                Console.Write("Please enter ship direction (Up, Down, Left or Right): ");
                string shipDirection = Console.ReadLine().ToUpper();

                switch (shipDirection)
                {
                    case "UP":
                        return ShipDirection.Up;
                    case "DOWN":
                        return ShipDirection.Down;
                    case "LEFT":
                        return ShipDirection.Left;
                    case "RIGHT":
                        return ShipDirection.Right;
                    default:
                        Console.Write("That wasn't a valud ship direction. Press any key to try again");
                        Console.ReadLine();
                        break;
                }
            }
        }


    }
}
