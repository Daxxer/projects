﻿using BattleShip.BLL.GameLogic;
using BattleShip.BLL.Requests;
using BattleShip.BLL.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BattleShip.UI
{
    public class Display
    {
        public void StartMessage()
        {
            Console.Clear();
            Console.WriteLine("Welcome to Battleship.");
            Console.WriteLine("2 Players are required for this game.");
            Console.Write("Press any key to continue...");
            Console.ReadKey();
        }

        public void DisplayGrid(Board board)
        {

            //look at the board's shot history and print grid with displayed shot history
            Console.Clear();
            Console.WriteLine("          A     B     C     D     E     F     G     H     I     J");
            Console.WriteLine();
            Console.WriteLine("       |-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|");
            for (int row = 1; row <= 10; row++)
            {
                if (row > 9)
                {
                    Console.Write($"  {row}   ");
                }
                else
                {
                    Console.Write($"   {row}   ");
                }
                

                for (int col = 1; col <= 10; col++)
                {
                    
                    Coordinate coord = new Coordinate(col, row);

                    if (board.ShotHistory.ContainsKey(coord))
                    {

                        Console.Write("|  ");

                        ShotHistory shotReturn = board.ShotHistory[coord];
                        switch (shotReturn)
                        {
                            case ShotHistory.Hit:
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.Write("H  ");
                                Console.ResetColor();
                                break;
                            case ShotHistory.Miss:
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                Console.Write("M  ");
                                Console.ResetColor();
                                break;
                            case ShotHistory.Unknown:
                                Console.Write("|     ");
                                break;
                            default:
                                break;
                        }
                    }
                    else
                    {
                        Console.Write("|     ");
                    }
                    
                }
                
                Console.WriteLine("|");
                Console.WriteLine("       |-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|");

            }

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            
        }
    }
}
